﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 0;
            var i = 0;
            var user = "0";

            Console.WriteLine("Hello, what's you name user?");
            user = Console.ReadLine();
            Console.WriteLine($"Thanks {user}, this program will show you a number of lines you wish to count");
            Console.WriteLine("How many lines would you like to see?");
            counter = int.Parse(Console.ReadLine());
            Console.WriteLine("Alright, well press enter whenever you're ready");
            Console.ReadKey();
            Console.Clear();

            while (i < counter)
            {
                var a = i++;
                if (i % 10 == 1)
                {
                    Console.WriteLine($"This is the {i}st instance of this line");
                }
                else if (i % 10 == 2)
                {
                    Console.WriteLine($"This is the {i}nd instance of this line");
                }
                else if (i % 10 == 3)
                {
                    Console.WriteLine($"This is the {i}rd instance of this line");
                }
                else
                {
                    Console.WriteLine($"This is the {i}th instance of this line");
                }
            }

        }
    }
}
